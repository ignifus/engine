#pragma once

#include <string>
#include <GL/glew.h>

namespace ignifusengine {
    struct File {
        std::string Path;
        std::string Content;

        File(std::string path, std::string content) : Path(path), Content(content){}
    };
}
