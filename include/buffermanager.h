#pragma once

#include <GL/glew.h>
#include "buffer.h"
#include "manager.h"

namespace ignifusengine {

    class BufferManager : public Manager<Buffer> {

        GLuint _VAO;

        GLuint findAvailablePosition(Buffer buffer);

    public:
        BufferManager(Logger* logger);
        ~BufferManager();

        void vertexBuffer(std::string handle, GLuint elementsPerEntity, GLuint sizePerElement, GLuint maxEntities, Entity* data, GLuint dataCount, GLenum drawType);

        void bindVertexBuffer(std::string handle);
        void unbindVertexBuffer();
        void vertexBufferAttribute(std::string handle, GLuint position, GLuint components, GLvoid* offset);

        void elementBuffer(std::string handle, GLuint elementsPerEntity, GLuint sizePerElement, GLuint maxEntities, GLuint* data, GLuint dataCount, GLenum drawType);
        void bindElementBuffer(std::string handle);
        void unbindElementBuffer();

    };

}
