#pragma once

#include <chrono>
#include <string>
#include <memory>
#include <iomanip>
#include <sstream>

namespace ignifusengine {

    typedef std::chrono::system_clock t_SChrono;
    typedef std::chrono::high_resolution_clock t_HRChrono;
    typedef std::chrono::milliseconds t_Milliseconds;

    class Timer
    {
    public:
        struct FrameData
        {
            double Ms;
            double Fps;

            FrameData(double ms, double fps)
            {
                Ms = ms;
                Fps = fps;
            }
        };

        Timer();

        void reset();

        std::string getToday() const;
        double getElapsedTime() const;

        FrameData& getFrameData();

        void calculateDelta();

    private:
        t_SChrono::time_point _Initial;
        t_SChrono::time_point _Today;

        double _DeltaTime;
        t_HRChrono::time_point _LastFrameTime;

        FrameData _FrameData;
    };
}
