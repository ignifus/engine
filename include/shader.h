#pragma once

#include <GL/glew.h>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace ignifusengine {
    struct Shader {
        GLuint Id;
        std::string VertexPath;
        std::string FragmentPath;

        Shader(GLuint id, std::string vertexPath, std::string fragmentPath);
        Shader();

        void set_uniform_1f(const GLchar*name, GLfloat value) const;
        void set_uniform_1i(const GLchar*name, GLuint value) const;
        void set_uniform_2f(const GLchar*name, glm::vec2& vector) const;
        void set_uniform_3f(const GLchar*name, glm::vec3& vector) const;
        void set_uniform_4f(const GLchar*name, glm::vec4& vector) const;

        void set_uniform_mat3(const GLchar* name, const glm::mat3& matrix) const;
        void set_uniform_mat3(GLuint id, const glm::mat3& matrix) const;

        void set_uniform_mat4(const GLchar* name, const glm::mat4& matrix) const;
        void set_uniform_mat4(GLuint id, const glm::mat4& matrix) const;

        void enable() const;
        static void disable();

        int get_uniform_location(const GLchar* name) const;
    };
}
