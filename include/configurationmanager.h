#pragma once

#include "commons.h"
#include "../dependencies/rapidxml/include/rapidxml.hpp"
#include "filemanager.h"
#include "logger.h"
#include "configuration.h"

namespace ignifusengine {

    class ConfigurationManager : public Manager<Configuration> {
    public:
        ConfigurationManager(Logger* logger, FileManager& fileManager);

        Configuration load(const std::string& handle, const std::string &filePath);

    private:
        FileManager& _FileManager;
    };
}
