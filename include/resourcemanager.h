#pragma once

#include <GL/glew.h>
#include <string>
#include <cstring>
#include <fstream>

#ifdef __linux__
#include <sys/sysinfo.h>
#include <sys/times.h>
#include <sys/vtimes.h>
#endif

#include "manager.h"

namespace ignifusengine {
    class ResourceManager : public Manager<GLuint64> {
    public:
        ResourceManager(Logger* logger);

        void init();

        struct SystemInfo
        {
            GLuint64  AvailableCpuMemory;
            GLuint64  TotalCpuMemory;

            GLuint64 AvailableGpuMemory;
            GLuint64 TotalGpuMemory;

            std::string CpuModel;
            std::string GpuModel;

            double CpuUsage;
        };

        struct CpuTimes{
            clock_t LastCPU, LastSysCPU, LastUserCPU;
            int NumProcessors;
        };

        template <class T, typename... Args>
        T* make(Args... args);

        GLuint64 getAllocatedMemory();
        ResourceManager::SystemInfo getSystemInfo();

        int getRamUsage();

    private:
        SystemInfo _SystemInfo;
        CpuTimes _CpuTimes;
        GLuint64 _AllocatedMemory;

        int parseRamUsageLine(char* line);

        void initCpuTimes();
        double getCpuUsage();

        std::string getCpuModel();
    };
}
