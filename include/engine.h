#pragma once

#include <iostream>
#include <sstream>
#include <iomanip>
#include <GL/glew.h>
#include "logger.h"
#include "configurationmanager.h"
#include "exceptionmanager.h"
#include "shadermanager.h"
#include "texturemanager.h"
#include "renderer2d.h"
#include "resourcemanager.h"

namespace ignifusengine {
    class Engine{
        typedef std::string (Engine::*ConsoleHandler)(const std::string (&params)[]);

        Timer _Timer;
        Logger _Logger;
        ResourceManager _ResourceManager;
        ExceptionManager _ExceptionManager;
        FileManager _FileManager;
        ConfigurationManager _ConfigurationManager;
        ShaderManager _ShaderManager;
        TextureManager _TextureManager;
        Renderer2d _Renderer2d;

        std::unordered_map<std::string, ConsoleHandler> _ConsoleHandlers;

        //TODO: Refactor renderer2d (EntityGroups, organize the addSegment, addEntity, entity lists, entity segments). Add to scenemanager
        //TODO: BufferManager Should be able to add or delete vertices to main buffer. Need to keep track of occupied offsets  GPUMemoryManager
        //TODO: After having a good API, make Qt List to modify entity data with GUI form.
        //TODO: Add throw engine exceptions on errors, and logging of process as neccesary
        //TODO: Consider init() methods because they reset an object instead of recreating it
        //TODO: Texture manager!
        //TODO: Object manager, tinyobjloader optimised, test the huge scene, buffer for each model is ok, consider the intel driver...
        //TODO: Engine halt auto window resize to trigger one repaint, engine recover reallocation, consider using pointers...
        //TODO: Console improvements - prevent typing on top of above lines - style commands - clean code - add commands
        //TODO: Destructors on destructible resources (GL..images..files..engine?)
        //TODO: GUI Commands are not passing an array of strings, rather one element with all the params. Should split on spaces
        //TODO: GUI Shader Reloading for script hotswapping
        //TODO: GUI Settings and OpenGL settings reloading
        //TODO: JSON File and parsing for HUD Quads generation
        //TODO: Entities draw true/false for hiding.
        //TODO: STD Allocator, CPU and GPU memory usage, CPU and GPU usage
        //TODO: 2D Currently supports quads only!
        //TODO: Qt Widget for List, should show entities with their fields and allow for modifying, on modify just call their setters on their ID's
        //TODO: Different cameras, different shaders as to draw wireframe and full render (3 in total, 1 full, 1 nolight, 1 wireframe)
        //TODO: 2D Text, 2D Renderer
        //TODO: 3D Renderer (Radiosity? Ray tracing? Deferred? Forward?)
        //TODO: Use Web requests and authenticate user when app starts, use a global object repository, with URL obj ID delivery. curlpp.
        //TODO: Report a bug button that sends email/to tracker software with details and exceptions list raised.
        //TODO: Help button
        //TODO: Should use locale files for easy text literals swapping. (LocaleManager)
        //TODO: GUI Progressbar, Issues/Notifications system for info, warnings or error.
        //TODO: Experiment with lightning
        //TODO: Experiment with shadows
        //TODO: Experiment with particles
        //TODO: Experiment with billboards
        //TODO: Experiment with materials
        //TODO: Quaternions for 3D rotations
        //TODO: Normal Mapping
        //TODO: Start implementing layers for 2D text (Menues, HUD)
        //TODO: Start implementing LUA support (Luajit-lualite)
        //TODO: Start implementing Sound support (OpenAL-GorillaAudio-tinysound)
        //TODO: Start implementing Physics support (Bullet)
        //TODO: Start implementing Configurations for both ENGINE and GUI Application
        //TODO: Start implementing Networking support (boost asio-tinynet)
        //TODO: Start implementing PostFX (framebuffers)
        //TODO: Start implementing animations
        //TODO: Start implementing video playing for cinematics
        //TODO: Start implementing an ingame interface support, (main menu, pause menu, and common interface menues for interaction)
        //TODO: Thread pooling, maybe multicore drawing using different buffers each with different data to avoid concurrency problems
        //TODO: Start optimizing with culling/conditional rendering
        //TODO: Start implementing controller support (input)
        //TODO: Start implementing a "Builder" to "Compile and ship current project" for game development
        //TODO: Start thinking about platforms
        //TODO: Final step to make a special engine, should be to invent a new way of 3D Rendering, next to innovative way of loading resources (objects, textures, files)
        //TODO: Try to define a very practical, clean and fast GUI for easy game development
        //TODO: Optimize every part of the system, everything should use as less resources as possible without reducing features
        //TODO: Comment code, document it and make a wiki
        //TODO: Document system requirements, Diagrams, dependencies, logo, libraries used, features
        //TODO: Use the resources folder in proyect
        //TODO: Use the docs folder in proyect

        enum Status : uint{
            Running,
            Halted,
            Crashed
        }_Status;

        void initOpenGl();

        std::string status(const std::string (&params)[]);
        std::string halt(const std::string (&params)[]);
        std::string wake(const std::string (&params)[]);
        std::string recover(const std::string (&params)[]);
        std::string stats(const std::string (&params)[]);
        std::string sysinfo(const std::string (&params)[]);
        std::string help(const std::string (&params)[]);
        void setupCommands();

    public:
        Engine(std::stringstream& outputStream, const std::string& configpath);

        bool start(GLint width, GLint height);
        void draw();

        void addOrEditDefaultQuad(std::string handle, const std::string& entityGroup, glm::vec3 position, glm::vec2 size, float rotation, const std::string& textureHandle);

        std::string getEngineUsages();

        std::string executeCommand(const std::string (&params)[]);

        Logger* getLogger() {
            return &_Logger;
        }
        Renderer2d* getRenderer2d() {
            return &_Renderer2d;
        }
        Status getStatus() {
            return _Status;
        }
        void crash(){
            _Status = Engine::Crashed;
        }
    };
}
