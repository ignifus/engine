#pragma once

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <unordered_map>
#include "engineexception.h"
#include "logger.h"
#include "file.h"
#include "manager.h"

namespace ignifusengine {
    class FileManager : public Manager<File> {

    public:
        FileManager(Logger* logger);

        File load(const std::string& filepath, std::string handle);

    };
}
