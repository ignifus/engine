#pragma once

#include <unordered_map>
#include "entitygroup.h"
#include "logger.h"
#include <glm/gtc/matrix_transform.hpp>

namespace ignifusengine {
    class Renderer2d {
        GLint _MaxEntities;
        GLint _Width;
        GLint _Height;

        glm::mat4 _ProjectionMatrix;

        std::map<std::string, EntityGroup> _EntityGroups;
        std::map<std::string, EntityGroup> _InstancedEntityGroups;

        GLuint _VAO;
        GLuint _VBO;
        GLuint _EBO;

        GLuint _VBOCurrentOffset;
        GLuint _EBOCurrentOffset;
        GLuint _TotalLoadedEntities;

        Logger* _Logger;

        void pushEntity(Entity2d entity);

    public:
        Renderer2d(Logger* logger);
        ~Renderer2d();

        void init();

        void draw();

        void addEntityGroup(std::string handle, EntityGroup entityGroup);

        EntityGroup* getEntityGroup(const std::string &handle);

        std::map<std::string, EntityGroup>* getEntityGroups();

        void addOrEditEntity(std::string handle, const std::string &entityGroup, Entity2d entity);

        void setWindowSize(GLint width, GLint height);

        void setMaxEntities(GLint maxEntities);
    };
}
