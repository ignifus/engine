#pragma once

#include <map>
#include "logger.h"

namespace ignifusengine {

    template <class T>
    class Manager{

    protected:
        std::map<std::string, T> _Elements;
        Logger* _Logger;

    public:
        Manager(Logger* logger) : _Logger(logger){}

        std::map<std::string, T>& getElements(){
            return _Elements;
        }

        T& getElement(const std::string& handle){
            return _Elements[handle];
        }

        T* getElementPointer(const std::string& handle){
            return &_Elements[handle];
        }
    };
}
