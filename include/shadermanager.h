#pragma once

#include <GL/glew.h>
#include <string>
#include "filemanager.h"
#include "shader.h"
#include "manager.h"

namespace ignifusengine {

    class ShaderManager : public Manager<Shader> {
    public:
        ShaderManager(Logger* logger, FileManager& fileManager);
        ~ShaderManager();

        Shader load(const std::string& shaderHandle, const std::string& vertexPath, const std::string& fragmentPath);

    private:
        FileManager& _FileManager;
    };
}
