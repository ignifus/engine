#pragma once

#include <GL/glew.h>
#include <string>

namespace ignifusengine {
    struct Texture{
        GLuint Id;
        std::string Name;
        std::string Type;
        GLenum Format;
        GLint Width;
        GLint Height;

        Texture(GLuint id, std::string name, std::string type, GLenum format, GLint width, GLint height) : Id(id), Name(name), Type(type), Format(format), Width(width), Height(height){

        }

        Texture(){}
    };
}
