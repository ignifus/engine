#pragma once

#include <string>
#include <sstream>
#include "engineexception.h"
#include "timer.h"

namespace ignifusengine {

    class Logger {

    public:
        struct Format{
            Format(unsigned short argsNum, const std::string& category){
                std::string s = "<span style=\" font-weight:600; color:#ff7129;\">" + category + "</span>  ";
                for (unsigned short i = 0; i < argsNum; i++)
                {
                    s.append("%s");
                }
                s.append("\n");

                FormatString = s;
            }

            std::string FormatString;
        };

        Logger( std::stringstream& stream, int level, Timer& timer);

        void setLoggingLevel(int level){
            _Level = level;
        }

        template<typename T, typename... Args>
        void info(Logger::Format f, const T &value, const Args &... args) {
            if(_Level == 0){
                _Stream << "<span style=\"color:#ffffff\">" << _Timer.getToday() << "</span> <span style=\" font-weight:600; color:#3ed6e8;\"> INFO</span>  ";
                auto *s = f.FormatString.c_str();
                while (*s) {
                    if (*s == '%' && *++s != '%') {
                        _Stream << value;
                        return LOG(++s, args...);
                    }
                    _Stream << *s++;
                }
                throw EngineException("Invalid format string: missing arguments for format provided." , "LOGGER", EngineException::Warning);
            }
        }

        template<typename T, typename... Args>
        void warning(Format f, const T& value, const Args&... args) {
            if(_Level <= 1) {
                _Stream << "<span style=\"color:#ffffff\">" << _Timer.getToday() << "</span> <span style=\" font-weight:600; color:#ffd552;\"> WARNING</span>  ";
                auto *s = f.FormatString.c_str();
                while (*s) {
                    if (*s == '%' && *++s != '%') {
                        _Stream << value;
                        return LOG(++s, args...);
                    }
                    _Stream << *s++;
                }
                throw EngineException("Invalid format string: missing arguments for format provided." , "LOGGER", EngineException::Warning);
            }
        }

        template<typename T, typename... Args>
        void error(Format f, const T& value, const Args&... args) {
            if(_Level <= 2) {
                _Stream << "<span style=\"color:#ffffff\">" << _Timer.getToday() << "</span> <span style=\" font-weight:600; color:#e31616;\"> ERROR</span>  ";
                auto *s = f.FormatString.c_str();
                while (*s) {
                    if (*s == '%' && *++s != '%') {
                        _Stream << value;
                        return LOG(++s, args...);
                    }
                    _Stream << *s++;
                }
                throw EngineException("Invalid format string: missing arguments for format provided." , "LOGGER", EngineException::Warning);
            }
        }

    private:

        void LOG(const std::string& str);

        std::stringstream& _Stream;
        int _Level;
        Timer& _Timer;
    };
}
