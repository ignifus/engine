#pragma once

#include <GL/glew.h>
#include <unordered_map>
#include "commons.h"
#include "engineexception.h"
#include "logger.h"
#include "manager.h"
#include "texture.h"

namespace ignifusengine {

    class TextureManager : public Manager<Texture> {

    public:
        TextureManager(Logger* logger);
        ~TextureManager();

        Texture load(const std::string& handle, const char* path, bool alpha);
    };
}
