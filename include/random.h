#pragma once

#include <random>

namespace ignifusengine {

    class Random {
        std::random_device _RandomDevice;
        std::mt19937 _MersenneEngine;
        std::uniform_int_distribution<> _UniformIntDistribution;
        std::uniform_real_distribution<> _UniformRealDistribution;
    public:
        Random(int min, int max);

        Random(double min, double max);

        int randomInt();

        double randomReal();
    };

}
