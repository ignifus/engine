#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>
#include "entity.h"
#include "texture.h"

namespace ignifusengine {
    struct Entity2d : public Entity{
        struct VertexData{
            glm::vec3 Positions;
            glm::vec4 Colors;
            glm::vec2 TexCoords;

            VertexData(glm::vec3 positions, glm::vec4 colors, glm::vec2 texcoords){
                Positions = positions;
                Colors = colors;
                TexCoords = texcoords;
            }
        };

        Entity2d(std::vector<VertexData> vertexDataList, glm::vec3 position, glm::vec2 size, GLfloat rotation, Texture texture);
        Entity2d();

        std::vector<VertexData> VertexDataList;
        glm::vec3 Position;
        glm::vec2 Size;
        GLfloat Rotation;

        Texture TextureObject;
    };
}
