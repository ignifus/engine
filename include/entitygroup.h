#pragma once

#include <vector>
#include <unordered_map>
#include <map>
#include "shader.h"
#include "entity2d.h"

namespace ignifusengine {
    class EntityGroup {
        std::map<std::string, Entity2d> _Entities;
        Shader* _Shader;
    public:
        EntityGroup(Shader* shader);
        EntityGroup();

        std::vector<Entity2d::VertexData> getDefaultVertexData();

        void addOrEditEntity(std::string handle, Entity2d entity);

        Entity2d* getEntity(const std::string& handle);

        std::map<std::string, Entity2d>* getEntities();;

        Shader* getShader();
    };
}
