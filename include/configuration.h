#pragma once

#include <GL/glew.h>

namespace ignifusengine {
    struct Configuration{
        std::unordered_map<std::string, std::string> Entries;
    };
}
