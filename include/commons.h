#pragma once

#include <sstream>
#include <string>
#include <iomanip>
#include <algorithm>
#include <cctype>
#include <vector>
#include <set>

namespace ignifusengine {
    inline bool toBool(std::string str) {
            std::transform(str.begin(), str.end(), str.begin(), ::tolower);
            std::istringstream is(str);
            bool b;
            is >> std::boolalpha >> b;
            return b;
    }

    inline std::vector<std::string> splitpath(
            const std::string& str
            , const std::set<char> delimiters);

    std::vector<std::string> splitpath(const std::string &str, const std::set<char> delimiters) {
            std::vector<std::string> result;

            char const* pch = str.c_str();
            char const* start = pch;
            for(; *pch; ++pch)
            {
                    if (delimiters.find(*pch) != delimiters.end())
                    {
                            if (start != pch)
                            {
                                    std::string str(start, pch);
                                    result.push_back(str);
                            }
                            else
                            {
                                    result.push_back("");
                            }
                            start = pch + 1;
                    }
            }
            result.push_back(std::string(start));

            return result;
    }
}
