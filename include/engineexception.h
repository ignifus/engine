#pragma once

#include <exception>
#include <string>

namespace ignifusengine {
    class EngineException : public std::exception
    {
    public:

        enum Severity
        {
            Warning = 1,
            Error = 2
        };

        explicit EngineException(const std::string& message, const std::string& component, Severity severity);

        const char* what() const throw() override;

        std::string getComponent();

        Severity getSeverity();
    private:
        std::string _Message;
        std::string _Component;
        Severity _Severity;
    };
}
