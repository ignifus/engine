#pragma once

#include <GL/glew.h>

namespace ignifusengine {
    struct Entity {

        GLuint _BufferPosition;

        Entity(GLuint bufferPosition);

    };
}
