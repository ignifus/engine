#pragma once

#include <string>
#include <map>
#include <vector>
#include "engineexception.h"
#include "logger.h"
#include "manager.h"

namespace ignifusengine {
    class ExceptionManager : public Manager<EngineException>
    {
    public:
        ExceptionManager(Logger* logger);

        void handle(EngineException ex);
    };
}
