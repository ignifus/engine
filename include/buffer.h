#pragma once

#include <GL/glew.h>
#include <vector>
#include <algorithm>
#include <string>
#include "entity.h"

namespace ignifusengine {

    struct Buffer {

        GLuint Id;
        GLuint MaxSize;
        GLuint ElementSize;
        GLuint EntityElementCount;
        GLenum DrawType;

        std::vector<GLuint> UsedPositions;

        Buffer(GLuint elementsPerEntity, GLuint sizePerElement, GLuint maxEntities,
               Entity *data, GLuint dataCount, GLenum drawType);

        Buffer();

        ~Buffer();

        void erase(GLuint position);
        void pushEntityData(GLuint position, GLvoid* data);
        void pushEntityElements(GLuint position, GLuint* indices);

        GLuint* getDefaultQuadIndices(GLuint position, GLuint* indices);
    };

}
