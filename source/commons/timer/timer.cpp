#include "../../../include/timer.h"

namespace ignifusengine {

    Timer::Timer() : _DeltaTime(0.0), _FrameData(0,0) {
        _Today = t_SChrono::now();
        _Initial = t_HRChrono::now();
    }

    void Timer::reset() {
        _Initial = t_HRChrono::now();
    }

    std::string Timer::getToday() const {
        auto today = std::chrono::system_clock::to_time_t(_Today);
        std::stringstream ss;
        ss << std::put_time(std::localtime(&today), "%Y-%m-%d %X");
        return ss.str();
    }

    double Timer::getElapsedTime() const {
        return static_cast<double>(std::chrono::duration_cast<t_Milliseconds>(
                t_HRChrono::now() - _Initial).count());
    }

    void Timer::calculateDelta() {
        auto currentFrame = t_HRChrono::now();
        _DeltaTime = static_cast<double>(std::chrono::duration_cast<t_Milliseconds>(
                currentFrame - _LastFrameTime).count());
        _LastFrameTime = currentFrame;

        _FrameData.Ms = _DeltaTime;
        _FrameData.Fps = 1 / _DeltaTime * 1000;
    }

    Timer::FrameData &Timer::getFrameData() {
        return _FrameData;
    }
}
