#include "../../../include/random.h"

namespace ignifusengine {

    Random::Random(int min, int max) : _MersenneEngine(_RandomDevice()), _UniformIntDistribution(min, max) {
    }

    Random::Random(double min, double max) : _MersenneEngine(_RandomDevice()), _UniformRealDistribution(min, max) {
    }

    int Random::randomInt() {
        return _UniformIntDistribution(_MersenneEngine);
    }

    double Random::randomReal() {
        return _UniformRealDistribution(_MersenneEngine);
    }
}
