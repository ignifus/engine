#include "../include/engine.h"

namespace ignifusengine {

    Engine::Engine(std::stringstream& outputStream, const std::string& configpath)
            : _Timer(),  _Logger(outputStream, 0, _Timer), _ResourceManager(&_Logger), _ExceptionManager(&_Logger),
              _FileManager(&_Logger), _ConfigurationManager(&_Logger, _FileManager), _ShaderManager(&_Logger, _FileManager), _TextureManager(&_Logger),
            _Renderer2d(&_Logger)
    {
        try {
            _ConfigurationManager.load("EngineConfiguration", configpath);

            _Renderer2d.setMaxEntities(stoi(_ConfigurationManager.getElement("EngineConfiguration").Entries["MaxEntities"]));
            _Logger.setLoggingLevel(stoi(_ConfigurationManager.getElement("EngineConfiguration").Entries["LoggingLevel"]));

            setupCommands();

            _Logger.info(Logger::Format(1, "ENGINE"), "Initialized engine.");
        }
        catch(EngineException ex){
            _ExceptionManager.handle(ex);
        }
    }

    bool Engine::start(GLint width, GLint height){
        try{
            _Logger.info(Logger::Format(1, "ENGINE"), "Starting Ignifus Engine.");

            initOpenGl();

            _ResourceManager.init();
            _Renderer2d.init();
            _Renderer2d.setWindowSize(width,height);

            _ShaderManager.load("MainShaders", "static/shaders/main.vert", "static/shaders/main.frag");
            EntityGroup es(_ShaderManager.getElementPointer("MainShaders"));

            Texture grassTile = _TextureManager.load("GrassTile", "static/textures/guachin.png", true);
            es.addOrEditEntity("Square1",
                               Entity2d(es.getDefaultVertexData(), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec2(64.0f, 64.0f),
                                        0.0f, grassTile));
            es.addOrEditEntity("Square2", Entity2d(es.getDefaultVertexData(), glm::vec3(150.0f, 0.0f, 0.0f),
                                                   glm::vec2(64.0f, 64.0f), 0.0f, grassTile));
            es.addOrEditEntity("Square3", Entity2d(es.getDefaultVertexData(), glm::vec3(300.0f, 0.0f, 0.0f),
                                                   glm::vec2(64.0f, 64.0f), 0.0f, grassTile));
            es.addOrEditEntity("Square4", Entity2d(es.getDefaultVertexData(), glm::vec3(450.0f, 0.0f, 0.0f),
                                                   glm::vec2(64.0f, 64.0f), 0.0f, grassTile));
            es.addOrEditEntity("Square5", Entity2d(es.getDefaultVertexData(), glm::vec3(600.0f, 0.0f, 0.0f),
                                                   glm::vec2(64.0f, 64.0f), 0.0f, grassTile));
            es.addOrEditEntity("Square6", Entity2d(es.getDefaultVertexData(), glm::vec3(750.0f, 0.0f, 0.0f),
                                                   glm::vec2(64.0f, 64.0f), 0.0f, grassTile));
            es.addOrEditEntity("Square7", Entity2d(es.getDefaultVertexData(), glm::vec3(900.0f, 0.0f, 0.0f),
                                                   glm::vec2(64.0f, 64.0f), 0.0f, grassTile));

            _Renderer2d.addEntityGroup("MainGroup", es);

            _Status = Engine::Running;
            _Logger.info(Logger::Format(1,"ENGINE"), "Engine started successfully!");
            return true;
        }
        catch(EngineException ex){
            _ExceptionManager.handle(ex);
            return false;
        }
    }

    void Engine::initOpenGl() {
        if(glewInit()){
            throw EngineException("Failed to initialize OpenGl.", "OPENGL", EngineException::Error);
        }

        auto majorVersion = _ConfigurationManager.getElement("EngineConfiguration").Entries["MajorVersion"];
        auto minorVersion = _ConfigurationManager.getElement("EngineConfiguration").Entries["MinorVersion"];

        if(glewIsSupported(("GL_VERSION_"+majorVersion+"_"+minorVersion).c_str())){
            _Logger.info(Logger::Format(1,"OPENGL"), "OpenGl Version "+majorVersion + "." + minorVersion+" supported!");
        }
        else{
            _Logger.warning(Logger::Format(1,"OPENGL"), "OpenGl Version "+majorVersion + "." + minorVersion+" not supported!");
        }

        if(toBool(_ConfigurationManager.getElement("EngineConfiguration").Entries["DepthTesting"]))
        {
            glEnable(GL_DEPTH_TEST);
            glDepthFunc(GL_LESS);
        }

        if(toBool(_ConfigurationManager.getElement("EngineConfiguration").Entries["FaceCulling"]))
        {
            glEnable(GL_CULL_FACE);
        }

        if(toBool(_ConfigurationManager.getElement("EngineConfiguration").Entries["AntiAliasing"]))
        {
            glEnable(GL_MULTISAMPLE);
        }

        if(toBool(_ConfigurationManager.getElement("EngineConfiguration").Entries["Blending"]))
        {
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        }

        if(toBool(_ConfigurationManager.getElement("EngineConfiguration").Entries["PolygonMode"]))
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        }

        GLint flags; glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
        if (flags & GL_CONTEXT_FLAG_DEBUG_BIT)
        {
            _Logger.info(Logger::Format(1,"OPENGL"), "OpenGl Debug context supported.");
        }
    }

    void Engine::draw() {
        try{
            glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT);

            _Timer.calculateDelta();
            _Renderer2d.draw();
        }
        catch(EngineException ex){
            _ExceptionManager.handle(ex);
        }
    }

    void Engine::addOrEditDefaultQuad(std::string handle, const std::string &entityGroup, glm::vec3 position, glm::vec2 size,
                                float rotation, const std::string &textureHandle) {
        _Renderer2d.addOrEditEntity(handle,
                                    entityGroup,
                                    Entity2d(
                                            _Renderer2d.getEntityGroups()->at(entityGroup).getDefaultVertexData(),
                                            position,
                                            size,
                                            rotation,
                                            _TextureManager.getElement(textureHandle)
                                    ));
    }

    std::string Engine::getEngineUsages(){
        try{
            auto frameData = _Timer.getFrameData();
            std::string s = "FPS: " + std::to_string(frameData.Fps).erase(2,8) +
                    " | MS: " + std::to_string(frameData.Ms).erase(2,8) +
                    " | RAM: " + std::to_string(_ResourceManager.getRamUsage() / 1024) + "MB" +
                    " | CPU: " + std::to_string(_ResourceManager.getSystemInfo().CpuUsage).erase(3,4) + "%";
            return s;
        }
        catch(EngineException ex){
            _ExceptionManager.handle(ex);
            return "ENGINE FAILED";
        }
    }

    void Engine::setupCommands() {
        _ConsoleHandlers.emplace("status", &Engine::status);
        _ConsoleHandlers.emplace("halt", &Engine::halt);
        _ConsoleHandlers.emplace("wake", &Engine::wake);
        _ConsoleHandlers.emplace("recover", &Engine::recover);
        _ConsoleHandlers.emplace("stats", &Engine::stats);
        _ConsoleHandlers.emplace("sysinfo", &Engine::sysinfo);
        _ConsoleHandlers.emplace("help", &Engine::help);
        _ConsoleHandlers.emplace("clear", nullptr);
    }

    std::string Engine::executeCommand(const std::string (&params)[]) {
        try{
            ConsoleHandler handler = _ConsoleHandlers.at(params[0]);
            return (this->*handler)(params);
        }
        catch(std::exception){
            return "Unrecognized command";
        }
    }

    std::string Engine::status(const std::string (&params)[]) {
        auto a = params[0];

        std::string output;

        switch (_Status){
            case Engine::Running: output+= "<span style=\" font-weight:600; color:#9CD58E;\">Running.</span> "; break;
            case Engine::Halted: output+= "<span style=\" font-weight:600; color:#ffd552;\">Halted.</span> "; break;
            case Engine::Crashed: output+= "<span style=\" font-weight:600; color:#e31616;\">Crashed.</span> "; break;
        }

        return output + std::to_string(_ExceptionManager.getElements().size()) + " exceptions raised.";
    }

    std::string Engine::halt(const std::string (&params)[]) {
        auto a = params[0];

        _Status = Engine::Halted;
        return "<span style=\" font-weight:600; color:#ffd552;\">Engine halted!</span>";
    }

    std::string Engine::wake(const std::string (&params)[]) {
        auto a = params[0];

        _Status = Engine::Running;
        return "<span style=\" font-weight:600; color:#9CD58E;\">Engine awaken!</span>";
    }

    std::string Engine::recover(const std::string (&params)[]) {
        auto a = params[0];

        _Status = Engine::Running;
        //TODO Reinitilization, do not erase groups.
        return "<span style=\" font-weight:600; color:#9CD58E;\">Engine recovered successfully.</span>";
    }

    std::string Engine::stats(const std::string (&params)[]) {
        auto a = params[0];

        return "<span style=\" font-weight:600; color:#ff7129;\">Files loaded: </span>" + std::to_string(_FileManager.getElements().size()) +
               "<br> <span style=\" font-weight:600; color:#ff7129;\">Shaders Loaded: </span>" + std::to_string(_ShaderManager.getElements().size()) +
               "<br> <span style=\" font-weight:600; color:#ff7129;\">Textures Loaded: </span>" + std::to_string(_TextureManager.getElements().size()) +
               "<br> <span style=\" font-weight:600; color:#ff7129;\">Total entities: </span>" + std::to_string(_Renderer2d.getEntityGroup("MainGroup")->getEntities()->size());
    }

    std::string Engine::sysinfo(const std::string (&params)[]) {
        auto a = params[0];

        auto systemInfo = _ResourceManager.getSystemInfo();
        return "<span style=\" font-weight:600; color:#ff7129;\">CPU:</span> " + systemInfo.CpuModel +
                "<br> <span style=\" font-weight:600; color:#ff7129;\">GPU: </span>" + systemInfo.GpuModel +
                "<br> <span style=\" font-weight:600; color:#ff7129;\">Total RAM: </span>" + std::to_string(systemInfo.TotalCpuMemory/(1024*1024)) + "MB" +
                "<br> <span style=\" font-weight:600; color:#ff7129;\">Free RAM: </span>" + std::to_string(systemInfo.AvailableCpuMemory/(1024*1024)) + "MB";
    }

    std::string Engine::help(const std::string (&params)[]) {
        auto a = params[0];

        std::string output = "Available commands: <br>";
        for(auto command : _ConsoleHandlers)
            output+= "<span style=\" font-weight:600; color:#ff7129;\">" + command.first + "</span> <br>";
        return output;
    }
}
