#include "../../../include/filemanager.h"

namespace ignifusengine {

    FileManager::FileManager(Logger* logger) : Manager<File>(logger) {

    }

    File FileManager::load(const std::string& filepath, std::string handle)
    {
        _Logger->info(Logger::Format(1,"FILEMGR"), "Reading file: " + filepath);

        std::ifstream in(filepath, std::ios::in | std::ios::binary);
        if (in)
        {
            std::ostringstream contents;
            contents << in.rdbuf();
            in.close();
            File file(filepath, contents.str());
            this->_Elements.emplace(handle, file);
            return file;
        }

        throw EngineException("Failed to read file: "  + filepath, "FILEMANAGER", EngineException::Warning);
    }
}
