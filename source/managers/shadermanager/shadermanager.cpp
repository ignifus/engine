#include "../../../include/shadermanager.h"

namespace ignifusengine {
    ShaderManager::ShaderManager(Logger* logger, FileManager& fileManager) : Manager<Shader>(logger), _FileManager(fileManager)
    {

    }

    ShaderManager::~ShaderManager()
    {
        for(auto shader : this->_Elements){
            glDeleteProgram(shader.second.Id);
        }
    }

    Shader ShaderManager::load(const std::string& shaderHandle, const std::string& vertexPath, const std::string& fragmentPath)
    {
        _Logger->info(Logger::Format(1,"SHADERMGR"), "Loading shader: " + shaderHandle);

        auto program = glCreateProgram();
        auto vertex = glCreateShader(GL_VERTEX_SHADER);
        auto fragment = glCreateShader(GL_FRAGMENT_SHADER);

        auto vertSourceString = _FileManager.load(vertexPath, shaderHandle+"Vert").Content;
        auto fragSourceString = _FileManager.load(fragmentPath, shaderHandle+"Frag").Content;

        auto* vertSource = vertSourceString.c_str();
        auto* fragSource = fragSourceString.c_str();

        //////////////////////////////////////////////////
        /////////////////////VERTEX///////////////////////
        //////////////////////////////////////////////////

        glShaderSource(vertex, 1, &vertSource, nullptr);
        glCompileShader(vertex);

        GLint result;
        glGetShaderiv(vertex, GL_COMPILE_STATUS, &result);

        if (result == GL_FALSE)
        {
            GLint length;
            glGetShaderiv(vertex, GL_INFO_LOG_LENGTH, &length);
            std::vector<char> error(length);
            glGetShaderInfoLog(vertex, length, &length, &error[0]);
            glDeleteShader(vertex);
            throw EngineException("Failed to compile vertex shader. " + vertexPath + " - " + std::string(&error[0]), "SHADERMGR", EngineException::Error);
        }

        //////////////////////////////////////////////////
        ////////////////////FRAGMENT//////////////////////
        //////////////////////////////////////////////////

        glShaderSource(fragment, 1, &fragSource, nullptr);
        glCompileShader(fragment);

        glGetShaderiv(fragment, GL_COMPILE_STATUS, &result);

        if (result == GL_FALSE)
        {
            GLint length;
            glGetShaderiv(fragment, GL_INFO_LOG_LENGTH, &length);
            std::vector<char> error(length);
            glGetShaderInfoLog(fragment, length, &length, &error[0]);
            glDeleteShader(fragment);
            throw EngineException("Failed to compile fragment shader. " + fragmentPath + " - " + std::string(&error[0]), "SHADERMGR", EngineException::Error);
        }

        /////////////////////////////////////////////////

        glAttachShader(program, vertex);
        glAttachShader(program, fragment);

        glLinkProgram(program);
        glValidateProgram(program);

        glDeleteShader(vertex);
        glDeleteShader(fragment);

        Shader shader(program,vertexPath,fragmentPath);
        _Elements.emplace(shaderHandle, shader);
        return shader;
    }
}
