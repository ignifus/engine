#include "../../../include/texturemanager.h"

#define STB_IMAGE_IMPLEMENTATION
#include "../../../dependencies/stb/include/stb_image.h"

namespace ignifusengine {
    TextureManager::TextureManager(Logger* logger) : Manager<Texture>(logger){

    }

    TextureManager::~TextureManager() {
        // TODO: Delete textures?
    }

    Texture TextureManager::load(const std::string& handle, const char* path, bool alpha)
    {
        _Logger->info(Logger::Format(1,"TEXTUREMGR"), "Loading texture: " + handle);

        GLuint textureID;
        glGenTextures(1, &textureID);

        GLenum format;
        GLint width, height, channels;
        auto* image = stbi_load(path, &width, &height, &channels, alpha ? STBI_rgb_alpha : STBI_rgb);

        if(!image)
            throw new EngineException("Failed to load texture: " + std::string(path), "TEXTUREMGR", EngineException::Warning);

        switch (channels)
        {
            case 1 : format = GL_ALPHA;     break;
            case 2 : format = GL_LUMINANCE; break;
            case 3 : format = GL_RGB;       break;
            case 4 : format = GL_RGBA;      break;
            default: format = GL_RGB;       break;
        }

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, alpha ? GL_CLAMP_TO_EDGE : GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, alpha ? GL_CLAMP_TO_EDGE : GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, image);
        glGenerateMipmap(GL_TEXTURE_2D);

        glBindTexture(GL_TEXTURE_2D, 0);

        stbi_image_free(image);

        Texture t(textureID, handle, "", format, width, height);
        _Elements[handle] = t;

        return t;
    }
}
