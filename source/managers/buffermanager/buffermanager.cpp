#include "../../../include/buffermanager.h"

namespace ignifusengine {

    BufferManager::BufferManager(Logger* logger) : Manager<Buffer>(logger) {
        glGenVertexArrays(1, &_VAO);
    }

    BufferManager::~BufferManager() {
        glDeleteVertexArrays(1, &_VAO);
    }

    void BufferManager::vertexBuffer(std::string handle, GLuint elementsPerEntity, GLuint sizePerElement, GLuint maxEntities,
                                Entity *data, GLuint dataCount, GLenum drawType) {

        glBindVertexArray(_VAO);
        _Elements.emplace(handle, Buffer(elementsPerEntity, sizePerElement, maxEntities, data, dataCount, drawType));
        glBindVertexArray(0);
    }

    void BufferManager::bindVertexBuffer(std::string handle) {
        glBindBuffer(GL_ARRAY_BUFFER, _Elements[handle].Id);
    }

    void BufferManager::unbindVertexBuffer() {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    void BufferManager::vertexBufferAttribute(std::string handle, GLuint position, GLuint components, GLvoid *offset) {
        Buffer b = _Elements[handle];

        glBindBuffer(GL_ARRAY_BUFFER, b.Id);
        glEnableVertexAttribArray(position);
        glVertexAttribPointer(position, components, GL_FLOAT, GL_FALSE, b.ElementSize, offset);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

}
