#include "../../../include/resourcemanager.h"

namespace ignifusengine {

    ResourceManager::ResourceManager(Logger *logger) : Manager<GLuint64>(logger){

    }

    void ResourceManager::init(){
        _SystemInfo.CpuModel = getCpuModel();
        _SystemInfo.GpuModel = reinterpret_cast<const char*>(glGetString(GL_RENDERER));

        initCpuTimes();
    }

    std::string ResourceManager::getCpuModel() {
#ifdef __linux__
        std::string line;
        std::ifstream finfo("/proc/cpuinfo");
        while(getline(finfo,line)) {
            std::stringstream str(line);
            std::string itype;
            std::string info;
            if ( getline( str, itype, ':' ) && getline(str,info) && itype.substr(0,10) == "model name" ) {
                return info;
            }
        }

        return "Error fetching processor info";
#endif
    }

    template<class T, typename... Args>
    T *ResourceManager::make(Args... args) {
        GLuint64 size = sizeof(T);
        std::string type = typeid(T).name();

        _AllocatedMemory+=size;
        _Elements.emplace(type, _AllocatedMemory);

        return new T(args...);
    }

    GLuint64 ResourceManager::getAllocatedMemory() {
        return _AllocatedMemory;
    }

    ResourceManager::SystemInfo ResourceManager::getSystemInfo() {
#ifdef __linux__
        struct sysinfo si;
        sysinfo(&si);

        _SystemInfo.TotalCpuMemory = si.totalram;
        _SystemInfo.AvailableCpuMemory = si.freeram;

        _SystemInfo.CpuUsage = getCpuUsage();

        return _SystemInfo;
#endif
    }

    int ResourceManager::parseRamUsageLine(char *line) {
#ifdef __linux__
        int i = (int)strlen(line);
        const char* p = line;
        while (*p <'0' || *p > '9') p++;
        line[i-3] = '\0';
        i = atoi(p);
        return i;
#endif
    }

    int ResourceManager::getRamUsage() {
#ifdef __linux__
        FILE* file = fopen("/proc/self/status", "r");
        int result = -1;
        char line[128];

        while (fgets(line, 128, file) != NULL){
            if (strncmp(line, "VmRSS:", 6) == 0){
                result = parseRamUsageLine(line);
                break;
            }
        }
        fclose(file);
        return result;
#endif
    }

    void ResourceManager::initCpuTimes(){
#ifdef __linux__
        FILE* file;
        struct tms timeSample;
        char line[128];

        _CpuTimes.LastCPU = times(&timeSample);
        _CpuTimes.LastSysCPU = timeSample.tms_stime;
        _CpuTimes.LastUserCPU = timeSample.tms_utime;

        file = fopen("/proc/cpuinfo", "r");
        while(fgets(line, 128, file) != NULL){
            if (strncmp(line, "processor", 9) == 0) _CpuTimes.NumProcessors++;
        }
        fclose(file);
#endif
    }

    double ResourceManager::getCpuUsage(){
#ifdef __linux__
        struct tms timeSample;
        clock_t now;
        double percent;

        now = times(&timeSample);
        if (now <= _CpuTimes.LastCPU || timeSample.tms_stime < _CpuTimes.LastSysCPU ||
            timeSample.tms_utime < _CpuTimes.LastUserCPU){
            //Overflow detection. Just skip this value.
            percent = -1.0;
        }
        else{
            percent = (timeSample.tms_stime - _CpuTimes.LastSysCPU) +
                      (timeSample.tms_utime - _CpuTimes.LastUserCPU);
            percent /= (now - _CpuTimes.LastCPU);
            percent /= _CpuTimes.NumProcessors;
            percent *= 100;
        }
        _CpuTimes.LastCPU = now;
        _CpuTimes.LastSysCPU = timeSample.tms_stime;
        _CpuTimes.LastUserCPU = timeSample.tms_utime;

        return percent;
#endif
    }
}
