#include "../../../include/configurationmanager.h"

namespace ignifusengine {
    ConfigurationManager::ConfigurationManager(Logger* logger, FileManager& fileManager) : Manager<Configuration>(logger), _FileManager(fileManager)
    {
    }

    Configuration ConfigurationManager::load(const std::string& handle, const std::string &filePath) {
        try {
            this->_Logger->info(Logger::Format(1,"CONFIGMGR"),"Loading configuration: " + filePath);
            auto xmlText = _FileManager.load(filePath.c_str(),"Configuration File").Content;

            rapidxml::xml_document<> doc;
            doc.parse<0>(&xmlText[0]);

            Configuration c;

            auto node = doc.first_node("Settings");
            for (auto node2 = node->first_node();
                 node2; node2 = node2->next_sibling())
            {
                c.Entries.emplace(node2->name(), node2->value());
            }

            _Elements.emplace(handle, c);
            return c;
        }
        catch(std::exception ex){
            throw EngineException("Failed to read config file.", "CONFIGMGR", EngineException::Error);
        }
    }
}
