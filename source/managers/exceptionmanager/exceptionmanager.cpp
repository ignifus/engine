#include "../../../include/exceptionmanager.h"

namespace ignifusengine {

    ExceptionManager::ExceptionManager(Logger* logger) : Manager<EngineException>(logger) {

    }

    void ExceptionManager::handle(EngineException ex) {
        this->_Logger->info(Logger::Format(1,"ENGINE"), "Exception thrown.. commencing error handling.");
        this->_Elements.emplace("Exception"+std::to_string(this->_Elements.size()), ex);

        switch (ex.getSeverity()){
            case EngineException::Warning:
                //TODO: Perform some action?

                _Logger->warning(Logger::Format(1,ex.getComponent()), ex.what());
                break;
            case EngineException::Error:
                //TODO: Perform some action?

                throw ex;
        }
    }
}
