#include "../../../include/shader.h"

namespace ignifusengine {
    Shader::Shader(uint id, std::string vertexPath, std::string fragmentPath) : Id(id), VertexPath(vertexPath), FragmentPath(fragmentPath){

    }

    Shader::Shader() {

    }

    void Shader::enable() const
    {
        glUseProgram(Id);
    }

    void Shader::disable()
    {
        glUseProgram(0);
    }

    GLint Shader::get_uniform_location(const GLchar* name) const
    {
        return glGetUniformLocation(Id, name);
    }

    void Shader::set_uniform_1f(const GLchar*name, GLfloat value) const
    {
        glUniform1f(get_uniform_location(name), value);
    }

    void Shader::set_uniform_1i(const GLchar*name, GLuint value) const
    {
        glUniform1i(get_uniform_location(name), value);
    }

    void Shader::set_uniform_2f(const GLchar*name, glm::vec2& vector)const
    {
        glUniform2f(get_uniform_location(name), vector.x, vector.y);
    }

    void Shader::set_uniform_3f(const GLchar*name, glm::vec3& vector) const
    {
        glUniform3f(get_uniform_location(name), vector.x, vector.y, vector.z);
    }

    void Shader::set_uniform_4f(const GLchar*name, glm::vec4& vector) const
    {
        glUniform4f(get_uniform_location(name), vector.x, vector.y, vector.z, vector.w);
    }

    void Shader::set_uniform_mat3(const GLchar* name, const glm::mat3& matrix) const
    {
        glUniformMatrix3fv(get_uniform_location(name), 1, GL_FALSE, glm::value_ptr(matrix));
    }

    void Shader::set_uniform_mat3(GLuint id, const glm::mat3& matrix) const
    {
        glUniformMatrix3fv(id, 1, GL_FALSE, glm::value_ptr(matrix));
    }

    void Shader::set_uniform_mat4(const GLchar* name, const glm::mat4& matrix) const
    {
        glUniformMatrix4fv(get_uniform_location(name), 1, GL_FALSE, glm::value_ptr(matrix));
    }

    void Shader::set_uniform_mat4(GLuint id, const glm::mat4& matrix) const
    {
        glUniformMatrix4fv(id, 1, GL_FALSE, glm::value_ptr(matrix));
    }
}
