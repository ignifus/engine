#include "../../../include/renderer2d.h"

namespace ignifusengine {

    Renderer2d::Renderer2d(Logger* logger) : _VBOCurrentOffset(0), _EBOCurrentOffset(0), _TotalLoadedEntities(0), _Logger(logger){

    }

    Renderer2d::~Renderer2d() {
        glDeleteBuffers(1, &_EBO);
        glDeleteBuffers(1, &_VBO);
        glDeleteVertexArrays(1, &_VAO);
    }

    void Renderer2d::init() {
        _Logger->info(Logger::Format(1,"RENDERER2D"), "Initializing renderer2d buffers.");

        glGenVertexArrays(1, &_VAO);
        glBindVertexArray(_VAO);

        glGenBuffers(1, &_VBO);
        glBindBuffer(GL_ARRAY_BUFFER, _VBO);
        glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(Entity2d::VertexData) * _MaxEntities, (GLvoid*)0, GL_STATIC_DRAW);

        glGenBuffers(1, &_EBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(GLuint) * _MaxEntities, (GLvoid*)0, GL_STATIC_DRAW);

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Entity2d::VertexData), reinterpret_cast<GLvoid*>(offsetof(Entity2d::VertexData, Positions)));

        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Entity2d::VertexData), reinterpret_cast<GLvoid*>(offsetof(Entity2d::VertexData, Colors)));

        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Entity2d::VertexData), reinterpret_cast<GLvoid*>(offsetof(Entity2d::VertexData, TexCoords)));

        glBindVertexArray(0);
    }

    void Renderer2d::draw() {
        GLubyte* indexOffset = 0;

        for(auto group : _EntityGroups){
            group.second.getShader()->enable();

            for(auto entity : *group.second.getEntities()){
                glm::mat4 model(1.0f);

                model = glm::translate(model, entity.second.Position);

                model = glm::translate(model, glm::vec3(entity.second.Size.x / 2.0f, entity.second.Size.y / 2.0f, 0));
                model = glm::rotate(model, glm::radians(entity.second.Rotation), glm::vec3(0.0f, 0.0f, 1.0f));
                model = glm::translate(model, -glm::vec3(entity.second.Size.x / 2.0f, entity.second.Size.y / 2.0f, 0));

                model = glm::scale(model, glm::vec3(entity.second.Size, 0.0f));

                auto MP = _ProjectionMatrix * model;

                group.second.getShader()->set_uniform_mat4("MP",MP);

                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, entity.second.TextureObject.Id);
                group.second.getShader()->set_uniform_1i("fragment_texture", 0);

                glBindVertexArray(_VAO);
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _EBO);
                glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)indexOffset);
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
                glBindVertexArray(0);

                indexOffset+=24;
            }

            group.second.getShader()->disable();
        }

        for(auto segment : _InstancedEntityGroups){
            //Draw logic for same texture instanced segments
        }
    }

    void Renderer2d::pushEntity(Entity2d entity) {
        glBindBuffer(GL_ARRAY_BUFFER, _VBO);
        for (int i = 0; i < entity.VertexDataList.size(); ++i) {
            glBufferSubData(GL_ARRAY_BUFFER, _VBOCurrentOffset, sizeof(Entity2d::VertexData), &(entity.VertexDataList[i]));
            _VBOCurrentOffset += sizeof(Entity2d::VertexData);
        }
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        GLuint indices[] = {_TotalLoadedEntities*4+0,
                            _TotalLoadedEntities*4+1,
                            _TotalLoadedEntities*4+3,
                            _TotalLoadedEntities*4+1,
                            _TotalLoadedEntities*4+2,
                            _TotalLoadedEntities*4+3};

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _EBO);
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, _EBOCurrentOffset, sizeof(indices), indices);
        _EBOCurrentOffset += sizeof(indices);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        _TotalLoadedEntities++;
    }

    void Renderer2d::addEntityGroup(std::string handle, EntityGroup entityGroup) {
        _Logger->info(Logger::Format(1,"RENDERER2D"), "Adding or editing group: " + handle);
        _EntityGroups.emplace(handle, entityGroup);

        for(auto entity : *entityGroup.getEntities())
            pushEntity(entity.second);
    }

    EntityGroup *Renderer2d::getEntityGroup(const std::string &handle) {
        return &_EntityGroups[handle];
    }

    std::map<std::string, EntityGroup> *Renderer2d::getEntityGroups() {
        return &_EntityGroups;
    }

    void Renderer2d::addOrEditEntity(std::string handle, const std::string &entityGroup, Entity2d entity) {
        _Logger->info(Logger::Format(1,"RENDERER2D"), "Adding or editing entity: " + handle + " in group: " + entityGroup);
        _EntityGroups[entityGroup].addOrEditEntity(handle, entity);

        pushEntity(entity);
    }

    void Renderer2d::setWindowSize(GLint width, GLint height) {
        _Width = width;
        _Height = height;

        _ProjectionMatrix = glm::ortho(0.0f, static_cast<float>(_Width), static_cast<float>(_Height), 0.0f, -1.0f, 1.0f);
    }

    void Renderer2d::setMaxEntities(GLint maxEntities) {
        _MaxEntities = maxEntities;
    }

}
