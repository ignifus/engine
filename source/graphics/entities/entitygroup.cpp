#include "../../../include/entitygroup.h"

namespace ignifusengine {

    EntityGroup::EntityGroup(Shader* shader) : _Shader(shader) {

    }


    EntityGroup::EntityGroup() : _Shader(nullptr){

    }

    std::vector<Entity2d::VertexData> EntityGroup::getDefaultVertexData() {
        std::vector<Entity2d::VertexData> vd;
        vd.push_back(Entity2d::VertexData(glm::vec3(1.0f,  0.0f, 0.0f),glm::vec4(1.0f,1.0f,1.0f,1.0f),glm::vec2(1.0f,1.0f)));
        vd.push_back(Entity2d::VertexData(glm::vec3(1.0f,  1.0f, 0.0f),glm::vec4(1.0f,1.0f,1.0f,1.0f),glm::vec2(1.0f,0.0f)));
        vd.push_back(Entity2d::VertexData(glm::vec3(0.0f,  1.0f, 0.0f),glm::vec4(1.0f,1.0f,1.0f,1.0f),glm::vec2(0.0f,0.0f)));
        vd.push_back(Entity2d::VertexData(glm::vec3(0.0f,  0.0f, 0.0f),glm::vec4(1.0f,1.0f,1.0f,1.0f),glm::vec2(0.0f,1.0f)));

        return vd;
    }

    void EntityGroup::addOrEditEntity(std::string handle, Entity2d entity) {
        _Entities[handle] = entity;
    }

    Entity2d *EntityGroup::getEntity(const std::string &handle) {
        return &_Entities[handle];
    }

    std::map<std::string, Entity2d> *EntityGroup::getEntities() {
        return &_Entities;
    }

    Shader *EntityGroup::getShader() {
        return _Shader;
    }

}
