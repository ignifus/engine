#include "../../../include/entity2d.h"

namespace ignifusengine {

    Entity2d::Entity2d(std::vector<VertexData> vertexDataList, glm::vec3 position, glm::vec2 size, GLfloat rotation, Texture texture) :
            Entity(0), VertexDataList(vertexDataList), Position(position), Size(size), Rotation(rotation), TextureObject(texture){

    }

    Entity2d::Entity2d() : Entity(0) {

    }

}
