#include "../../../include/buffer.h"

namespace ignifusengine {
    Buffer::Buffer(GLuint elementsPerEntity, GLuint sizePerElement, GLuint maxEntities, Entity* data, GLuint dataCount,
                   GLenum drawType) : MaxSize(elementsPerEntity*sizePerElement*maxEntities), ElementSize(sizePerElement),
                                      EntityElementCount(elementsPerEntity), DrawType(drawType)
    {
        glGenBuffers(1, &Id);
        glBindBuffer(GL_ARRAY_BUFFER, Id);
        glBufferData(GL_ARRAY_BUFFER, MaxSize, data, drawType);

        for (GLuint i = 0; i < dataCount; ++i) {
            data->_BufferPosition = i;
            UsedPositions.push_back(i);
            data++;
        }
    }

    Buffer::Buffer() {

    }

    Buffer::~Buffer() {
        glDeleteBuffers(1, &Id);
    }

    void Buffer::erase(GLuint position) {
        UsedPositions.erase(std::remove(UsedPositions.begin(), UsedPositions.end(), position), UsedPositions.end());
    }

    void Buffer::pushEntityData(GLuint position, GLvoid* data) {
        glBindBuffer(GL_ARRAY_BUFFER, Id);
        glBufferSubData(GL_ARRAY_BUFFER, position*ElementSize, EntityElementCount*ElementSize, data);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        UsedPositions.push_back(position);
    }

    void Buffer::pushEntityElements(GLuint position, GLuint* indices) {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Id);
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, position * ElementSize, EntityElementCount*ElementSize, indices);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        UsedPositions.push_back(position);
    }

    GLuint* Buffer::getDefaultQuadIndices(GLuint position, GLuint* indices){
        indices[0] = position*EntityElementCount+0;
        indices[1] = position*EntityElementCount+1;
        indices[2] = position*EntityElementCount+3;
        indices[3] = position*EntityElementCount+1;
        indices[4] = position*EntityElementCount+2;
        indices[5] = position*EntityElementCount+3;

        return indices;
    }
}
