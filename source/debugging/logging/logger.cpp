#include "../../../include/logger.h"

namespace ignifusengine {
    Logger::Logger(std::stringstream& stream, int level, Timer& timer) : _Stream(stream), _Level(level), _Timer(timer){

    }

    void Logger::LOG(const std::string& str) {
        auto *s = str.c_str();
        while (*s) {
            if (*s == '%' && *++s != '%')
                throw EngineException("Invalid format string: missing arguments for format provided for: " + str, "LOGGER", EngineException::Warning);
            _Stream << *s++;
        }
    }
}
