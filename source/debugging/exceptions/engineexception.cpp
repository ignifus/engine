#include "../../../include/engineexception.h"

namespace ignifusengine {

    EngineException::EngineException(const std::string &message, const std::string &component,
                                     EngineException::Severity severity) :
            _Message(message), _Component(component), _Severity(severity)
    {
    }

    const char *EngineException::what() const throw(){
        return _Message.c_str();
    }

    std::string EngineException::getComponent() {
        return _Component;
    }

    EngineException::Severity EngineException::getSeverity() {
        return _Severity;
    }
}
