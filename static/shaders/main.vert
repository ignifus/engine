#version 300 es

precision mediump float;

layout (location = 0) in vec3 position;
layout (location = 1) in vec4 color;
layout (location = 2) in vec2 texcoords;

out vec4 fragment_color;
out vec2 fragment_texcoords;

uniform mat4 MP;

void main()
{
    gl_Position = MP * vec4(position, 1.0);

    fragment_color = color;
    fragment_texcoords = vec2(texcoords.x, 1.0 - texcoords.y);
}
