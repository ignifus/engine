#version 300 es

precision mediump float;

in vec4 fragment_color;
in vec2 fragment_texcoords;

out vec4 color;

uniform sampler2D fragment_texture;

void main()
{
	color = texture(fragment_texture, fragment_texcoords) * fragment_color;
}